package com.atlassian.oauth.testing;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;
import com.atlassian.oauth.consumer.ConsumerToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.google.common.collect.ImmutableSet;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthMessage;
import net.oauth.OAuthServiceProvider;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Map;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.Matchers.argThat;

public class Matchers {
    public static <T> Matcher<? super T> equalTo(T o) {
        return org.hamcrest.Matchers.equalTo(o);
    }

    public static Matcher<? super OAuthConsumer> equalTo(OAuthConsumer consumer) {
        return samePropertyValuesAs(consumer);
    }

    public static Matcher<? super Consumer> equalTo(Consumer consumer) {
        return samePropertyValuesAs(consumer);
    }

    public static Matcher<? super OAuthServiceProvider> equalTo(OAuthServiceProvider oauthServiceProvider) {
        return samePropertyValuesAs(oauthServiceProvider);
    }

    public static Matcher<? super ServiceProvider> equalTo(ServiceProvider serviceProvider) {
        return samePropertyValuesAs(serviceProvider);
    }

    public static Matcher<? super OAuthAccessor> equalTo(OAuthAccessor accessor) {
        return samePropertyValuesAs(accessor);
    }

    public static Matcher<? super ServiceProviderToken> equalTo(ServiceProviderToken token) {
        // Hamcrest's is/equalTo matcher uses Object.equals so Consumer and Session properties
        // need to be explicitly checked

        Consumer consumer = token.getConsumer();
        Matcher<?> consumerMatcher =
                consumer == null ? nullValue() : samePropertyValuesAs(consumer);

        ServiceProviderToken.Session session = token.getSession();
        Matcher<?> sessionMatcher =
                session == null ? nullValue() : samePropertyValuesAs(session);

        return allOf(hasProperty("consumer", consumerMatcher),
                hasProperty("session", sessionMatcher),
                hasProperty("authorization", is(token.getAuthorization())),
                hasProperty("callback", is(token.getCallback())),
                hasProperty("creationTime", is(token.getCreationTime())),
                hasProperty("timeToLive", is(token.getTimeToLive())),
                hasProperty("user", is(token.getUser())),
                hasProperty("verifier", is(token.getVerifier())),
                hasProperty("version", is(token.getVersion())),
                hasProperty("properties", is(token.getProperties())),
                hasProperty("token", is(token.getToken())),
                hasProperty("tokenSecret", is(token.getTokenSecret())));
    }

    public static Matcher<? super ConsumerToken> equalTo(ConsumerToken token) {
        // Hamcrest's is/equalTo matcher uses Object.equals so Consumer properties
        // need to be explicitly checked

        // Note: token properties excluded from match as they do not survive conversion
        // from OAuthAccessors
        return allOf(hasProperty("consumer", samePropertyValuesAs(token.getConsumer())),
                hasProperty("token", is(token.getToken())),
                hasProperty("tokenSecret", is(token.getTokenSecret())));
    }

    public static Matcher<? super OAuthMessage> equalTo(OAuthMessage message) {
        return samePropertyValuesAs(message);
    }

    public static Matcher<? super Request> equalTo(Request request) {
        return samePropertyValuesAs(request);
    }

    public static final <K, V> Matcher<Map<? extends K, ? extends V>> hasEntries(Map<K, V> map) {
        ImmutableSet.Builder<Matcher<? super Map<? extends K, ? extends V>>> builder = ImmutableSet.builder();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            builder.add(hasEntry(entry.getKey(), entry.getValue()));
        }
        return allOf(builder.build());
    }

    public static Matcher<? super Map<String, Object>> mapWithKeys(final Matcher<? super String> keyMatcher) {
        return new TypeSafeDiagnosingMatcher<Map<String, Object>>() {
            @Override
            protected boolean matchesSafely(Map<String, Object> item, Description mismatchDescription) {
                boolean matches = true;
                for (String key : item.keySet()) {
                    if (!keyMatcher.matches(key)) {
                        if (!matches) {
                            mismatchDescription.appendText(", ");
                        }
                        mismatchDescription
                                .appendText("key \"")
                                .appendText(key)
                                .appendText("\" ");
                        keyMatcher.describeMismatch(key, mismatchDescription);
                        matches = false;
                    }
                }
                return matches;
            }

            public void describeTo(Description description) {
                description.appendText("Map with all keys with ").appendDescriptionOf(keyMatcher);
            }
        };
    }

    public static Matcher<? super String> withStringLength(final Matcher<? super Integer> lengthMatcher) {
        return new TypeSafeDiagnosingMatcher<String>() {
            @Override
            protected boolean matchesSafely(String item, Description mismatchDescription) {
                int length = item.length();
                if (!lengthMatcher.matches(length)) {
                    lengthMatcher.describeMismatch(length, mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description) {
                description.appendText("string length of ").appendDescriptionOf(lengthMatcher);
            }
        };
    }

    public static Map<String, Object> hasFieldError(final String key, final String error) {
        return (Map<String, Object>) argThat(new TypeSafeDiagnosingMatcher<Map<String, Object>>() {
            @SuppressWarnings("unchecked")
            @Override
            protected boolean matchesSafely(Map<String, Object> item, Description mismatchDescription) {
                Object message = ((Map<String, Object>) item.get("fieldErrorMessages")).get(key);
                return message != null && error.equals(message.toString());
            }

            public void describeTo(Description description) {
                description.appendText("map containing[\"fieldErrorMessages\"-><{")
                        .appendValue(key)
                        .appendText("=")
                        .appendValue(error)
                        .appendText("}>]");
            }
        });
    }
}
