package com.atlassian.webdriver.oauth.page;

import com.atlassian.pageobjects.Page;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

/**
 * Abstract representation of an OAuth page.
 *
 * @since v1.9.0
 */
public abstract class AbstractOAuthPage implements Page {
    protected static final String BASE_URL = System.getProperty("baseurl") != null
            ? System.getProperty("baseurl") : "http://localhost:8080/oauth";

    protected static final String SERVLET_BASE_URL = "/plugins/servlet";
    protected static final String REQUEST_TOKEN_URL = SERVLET_BASE_URL + "/oauth/request-token";
    protected static final String AUTHORIZE_URL = SERVLET_BASE_URL + "/oauth/authorize";

    @Inject
    protected WebDriver driver;

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public String getPageSource() {
        return driver.getPageSource();
    }
}
