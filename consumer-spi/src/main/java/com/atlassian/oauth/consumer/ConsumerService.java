package com.atlassian.oauth.consumer;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Request;
import com.atlassian.oauth.ServiceProvider;

import javax.annotation.Nullable;
import java.net.URI;
import java.security.PrivateKey;
import java.util.List;

/**
 * <p>Provides creation and storage of the host applications {@link Consumer} instance.  To retrieve the application
 * {@code Consumer} object use the {@link #getConsumer()} method.  If the {@code Consumer} object has not yet been
 * created for the host application, it will be created on request.  An administrator may want to customize some of the
 * consumer information provided to service providers, this can be accomplished by calling the
 * {@link #updateHostConsumerInformation(String, String, URI)} method with the desired information.</p>
 *
 * <p>The consumer {@link PrivateKey} is not made public. This is intentional as it is a highly security sensitive
 * piece of data and should not be exposed to the outside world in any way. To send request signed with the consumers
 * private key, use the {@link #sign(Request, ServiceProvider)} method.</p>
 */
public interface ConsumerService {
    /**
     * Returns the consumer information for the host application.  If it does not exist yet, it will be created and
     * stored for later uses.
     *
     * @return the consumer information for the host application
     * @throws ConsumerCreationException thrown if there are problem creating the {@code Consumer} object, such as
     *                                   a problem generating the public/private RSA key pair.
     */
    Consumer getConsumer() throws ConsumerCreationException;

    /**
     * Returns the consumer information for the service.  If there is no consumer information for the service,
     * {@code null} is returned.
     *
     * @param service Name of the service to get the consumer information for
     * @return the consumer information for the service, or {@code null} if there is no consumer information for the service
     */
    Consumer getConsumer(String service);

    /**
     * Returns the consumer information for the consumer key.  If there is no consumer information with that key,
     * {@code null} is returned.
     *
     * @param consumerKey Consumer key of the consumer information to return
     * @return the consumer information corresponding to the consumer key, or {@code null} if there is no consumer
     * information with that key
     */
    Consumer getConsumerByKey(String consumerKey);

    /**
     * Returns all the service provider assigned consumer information.
     *
     * @return all the service provider assigned consumer information
     */
    Iterable<Consumer> getAllServiceProviders();

    /**
     * Add the consumer information for the service.
     *
     * @param service    Name of the service the consumer information is associated with
     * @param consumer   Consumer inforamtion to store
     * @param privateKey the key to use when signing request
     */
    void add(String service, Consumer consumer, PrivateKey privateKey);

    /**
     * Add the consumer information for the service.
     *
     * @param service      Name of the service the consumer information is associated with
     * @param consumer     Consumer inforamtion to store
     * @param sharedSecret the shared secret to use when signing requests
     */
    void add(String service, Consumer consumer, String sharedSecret);

    /**
     * Remove the consumer information, and any tokens generated using the consumer information.
     *
     * @param consumerKey key of the consumer to remove along with any associated tokens
     * @throws IllegalArgumentException if the service that is attempted to be removed is the host service
     */
    void removeConsumerByKey(String consumerKey);

    /**
     * Updates the name, description and callback URI of the host application.  The {@code name} parameter is
     * required to be a non-empty String.  The {@code description} and {@code callback} URI are allowed to
     * be null.
     *
     * @param name        Name to identify the host application as to service providers
     * @param description Description of the host application to service providers
     * @param callback    {@code URI} to redirect the user to after the service provider has obtained authorization from
     *                    the user
     * @return updated {@code Consumer} information
     */
    Consumer updateHostConsumerInformation(String name, @Nullable String description, @Nullable URI callback);

    /**
     * Creates and returns a new request that is the same as the given {@code request} parameter, signed with the
     * host applications private key and with all the required OAuth parameters added.
     *
     * NB: Use this method to sign requests to get a request token or access token.
     *
     * @param request         request to be signed
     * @param serviceProvider the service provider the OAuth request is being sent to
     * @return the original {@code request} as a signed OAuth request
     */
    Request sign(Request request, ServiceProvider serviceProvider);

    /**
     * Creates and returns a new request that is the same as the given {@code request} parameter, signed with the
     * specified {@code service} consumer information and with all the required OAuth parameters added.
     *
     * NB: Use this method to sign requests to get a request token or access token.
     *
     * @param request         request to be signed
     * @param consumerKey     the key of the consumer to be used to sign the request
     * @param serviceProvider the service provider the OAuth request is being sent to
     * @return the original {@code request} as a signed OAuth request
     * @throws OAuthConsumerNotFoundException thrown if there are no consumers with the given consumer key
     */
    Request sign(Request request, String consumerKey, ServiceProvider serviceProvider);

    /**
     * Creates and returns a new request that is the same as the {@code request} parameter, signed with the host
     * applications private key and with all the required OAuth parameters and the OAuth token information added.
     *
     * NB: Use this method to sign requests after you have obtained a token from the service provider.
     *
     * @param request         request to be signed
     * @param serviceProvider the service provider the OAuth request is being sent to
     * @param token           OAuth token to use when signing the request
     * @return the original {@code request} as a signed OAuth request
     * @throws OAuthConsumerNotFoundException thrown if the consumer that was used to create the token no longer exists
     */
    Request sign(Request request, ServiceProvider serviceProvider, ConsumerToken token);
}
