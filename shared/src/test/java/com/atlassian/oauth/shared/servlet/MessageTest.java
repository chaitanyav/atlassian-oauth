package com.atlassian.oauth.shared.servlet;

import com.atlassian.sal.api.message.I18nResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.Serializable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageTest {
    @Mock
    I18nResolver resolver;

    @Test
    public void verifyThatToStringUsesResolver() {
        Serializable[] params = new Serializable[]{"a parameter"};
        Message message = new Message(resolver, "message.key", params);
        when(resolver.getText("message.key", params)).thenReturn("A message with a parameter");

        assertThat(message.toString(), is(equalTo("A message with a parameter")));
        verify(resolver).getText("message.key", params);
    }
}
