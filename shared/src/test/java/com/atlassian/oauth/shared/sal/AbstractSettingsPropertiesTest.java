package com.atlassian.oauth.shared.sal;

import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class AbstractSettingsPropertiesTest {
    private static final Properties PROPERTIES = new Properties();

    static {
        PROPERTIES.put("value", "something");
    }

    @Test
    public void assertThatAsPropertiesReturnsSamePropertiesAsPassedToConstructor() {
        SimpleSettingsProperties settingsProperties = new SimpleSettingsProperties(PROPERTIES);

        assertThat(settingsProperties.asProperties(), is(equalTo(PROPERTIES)));
    }

    @Test
    public void assertThatPutValueShowsUpInProperties() {
        SimpleSettingsProperties settingsProperties = new SimpleSettingsProperties();
        settingsProperties.putValue("something");

        assertThat(settingsProperties.asProperties(), is(equalTo(PROPERTIES)));
    }

    @Test
    public void assertThatCanGetValueThatWasPut() {
        SimpleSettingsProperties settingsProperties = new SimpleSettingsProperties();
        settingsProperties.putValue("something");

        assertThat(settingsProperties.getValue(), is(equalTo("something")));
    }

    static final class SimpleSettingsProperties extends AbstractSettingsProperties {
        public SimpleSettingsProperties() {
        }

        public SimpleSettingsProperties(Properties properties) {
            super(properties);
        }

        public String getValue() {
            return get("value");
        }

        public void putValue(String value) {
            put("value", value);
        }
    }
}
