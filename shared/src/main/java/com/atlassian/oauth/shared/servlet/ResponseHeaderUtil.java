package com.atlassian.oauth.shared.servlet;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

public final class ResponseHeaderUtil
{
    private ResponseHeaderUtil()
    {
        // do no instantiate
    }

    public static void preventCrossFrameClickJacking(@Nonnull final HttpServletResponse response)
    {
        Preconditions.checkNotNull(response, "response").setHeader("X-Frame-Options", "SAMEORIGIN");
    }
}
