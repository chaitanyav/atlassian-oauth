package com.atlassian.oauth.shared.servlet;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

/**
 * Template Renderer automatically escapes all text entered into the page.  In some cases, like text returned from
 * {@link I18nResolver} we don't want it to be escaped.  Using this utility we can specify on a case-by-case basis
 * the values we don't want escaped using {@code $unescape.html($i18nResolver.getText("message.containing.html"))}.
 */
public class Unescaper {
    /**
     * Just returns the {@code text} parameter.  Because this method is annotated with {@code @HtmlSafe} the returned
     * value will not be escaped when it is inserted into the rendered template.
     *
     * @param text Text that we don't want escaped
     * @return {@code text} unmodified
     */
    @HtmlSafe
    public String html(String text) {
        return text;
    }
}
