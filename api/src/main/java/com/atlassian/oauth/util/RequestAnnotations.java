package com.atlassian.oauth.util;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Managed annotations on the request to mark certain information to downstream parties
 *
 * @since 1.2
 */
public class RequestAnnotations {
    private static final String OAUTH_REQUEST_FLAG = "com.atlassian.oath.request-flag";
    private static final String OAUTH_CONSUMER_KEY = "com.atlassian.oath.consumer-key";

    /**
     * @param req The servlet request to test, cannot be null;
     * @return If the request was authenticated via OAuth
     * @throws NullPointerException If the req variable is null
     */
    public static boolean isOAuthRequest(HttpServletRequest req) throws NullPointerException {
        checkNotNull(req);
        return req.getAttribute(OAUTH_REQUEST_FLAG) != null;
    }

    /**
     * Annotates the servlet request as one having been authenticated via OAuth.  Subsequent calls on the same
     * request will have the same result.
     *
     * @param req The servlet request to annotate
     * @throws NullPointerException If the req variable is null
     */
    public static void markAsOAuthRequest(HttpServletRequest req) throws NullPointerException {
        checkNotNull(req);
        req.setAttribute(OAUTH_REQUEST_FLAG, "true");
    }

    /**
     * Gets the consumer key that the given OAuth request has been authenticated on.
     * Calling this method on a non-OAuth request will result in {@link IllegalStateException}.
     * If unsure, simply call {@link #isOAuthRequest(javax.servlet.http.HttpServletRequest)} beforehand.
     *
     * @param req the servlet request
     * @return consumer key
     * @since 1.6
     */
    public static String getOAuthConsumerKey(HttpServletRequest req) {
        checkNotNull(req);
        checkState(isOAuthRequest(req), "cannot get OAuth consumer key out of non-OAuth request!");
        return (String) req.getAttribute(OAUTH_CONSUMER_KEY);
    }

    /**
     * Annotates the servlet request as one having been authenticated via OAuth using the consumerKey given.
     *
     * @param req         the servlet request
     * @param consumerKey consumer key that the request has been authenticated on
     * @since 1.6
     */
    public static void setOAuthConsumerKey(HttpServletRequest req, String consumerKey) {
        checkNotNull(req);
        checkNotNull(consumerKey);
        req.setAttribute(OAUTH_CONSUMER_KEY, consumerKey);
    }
}
