package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Token;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.OAuthConverter;
import com.atlassian.oauth.serviceprovider.internal.TokenFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import net.oauth.OAuth.Parameter;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.server.OAuthServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.serviceprovider.internal.servlet.OAuthProblemUtils.logOAuthProblem;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static net.oauth.OAuth.OAUTH_CALLBACK_CONFIRMED;
import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.OAUTH_TOKEN_SECRET;
import static net.oauth.OAuth.Problems.CONSUMER_KEY_UNKNOWN;
import static net.oauth.OAuth.Problems.OAUTH_PARAMETERS_REJECTED;
import static net.oauth.OAuth.Problems.OAUTH_PROBLEM_ADVICE;
import static net.oauth.OAuth.Problems.PARAMETER_REJECTED;
import static net.oauth.OAuth.Problems.PERMISSION_DENIED;
import static net.oauth.OAuth.formEncode;
import static net.oauth.server.OAuthServlet.handleException;

public class RequestTokenServlet extends TransactionalServlet {
    @VisibleForTesting
    static final String INVALID_CALLBACK_ADVICE =
            "As per OAuth spec version 1.0 Revision A Section 6.1 <http://oauth.net/core/1.0a#auth_step1>, the " +
                    "oauth_callback parameter is required and must be either a valid, absolute URI using the http or https scheme, " +
                    "or 'oob' if the callback has been established out of band. The following invalid URI was supplied '%s'";

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final ApplicationProperties applicationProperties;
    private final TokenFactory factory;
    private final OAuthValidator validator;
    private final OAuthConverter converter;
    private final ServiceProviderConsumerStore consumerStore;
    private final ServiceProviderTokenStore tokenStore;

    public RequestTokenServlet(ServiceProviderConsumerStore consumerStore,
                               @Qualifier("tokenStore") ServiceProviderTokenStore tokenStore,
                               TokenFactory factory,
                               OAuthValidator validator,
                               OAuthConverter converter,
                               ApplicationProperties applicationProperties,
                               TransactionTemplate transactionTemplate) {
        super(transactionTemplate);
        this.consumerStore = checkNotNull(consumerStore, "consumerStore");
        this.tokenStore = checkNotNull(tokenStore, "tokenStore");
        this.factory = checkNotNull(factory, "factory");
        this.validator = checkNotNull(validator, "validator");
        this.converter = checkNotNull(converter, "converter");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    @Override
    public void doPostInTransaction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            OAuthMessage message = OAuthServlet.getMessage(request, null);
            message.requireParameters(OAUTH_CONSUMER_KEY);
            Consumer consumer = consumerStore.get(message.getConsumerKey());
            if (consumer == null) {
                throw new OAuthProblemException(CONSUMER_KEY_UNKNOWN);
            }
            if (!consumer.getThreeLOAllowed()) {
                throw new OAuthProblemException(PERMISSION_DENIED);
            }

            URI callback;
            Version version;
            if (message.getParameter(OAUTH_CALLBACK) != null) {
                callback = callbackToUri(message.getParameter(OAUTH_CALLBACK));
                version = V_1_0_A;
            } else {
                callback = null;
                version = V_1_0;
            }
            try {
                validator.validateMessage(message, new OAuthAccessor(converter.toOAuthConsumer(consumer)));
            } catch (OAuthProblemException ope) {
                logOAuthProblem(message, ope, log);
                throw ope;
            }
            Token token = tokenStore.put(factory.generateRequestToken(consumer, callback, message, version));
            // We form encode the output, but it's not standard form encoding, it's OAuth form encoding.  The main
            // difference seems to be that OAuth doesn't encode spaces as '+'. See
            // <http://groups.google.com/group/oauth/browse_thread/thread/ef2455dc89546222>.  This means we can't use
            // application/x-www-form-urlencoded as you might expect, we need to use text/plain
            response.setContentType("text/plain");

            OutputStream out = response.getOutputStream();
            ImmutableList.Builder<Parameter> builder = ImmutableList.builder();
            builder.add(new Parameter(OAUTH_TOKEN, token.getToken()));
            builder.add(new Parameter(OAUTH_TOKEN_SECRET, token.getTokenSecret()));
            if (V_1_0_A.equals(version)) {
                builder.add(new Parameter(OAUTH_CALLBACK_CONFIRMED, "true"));
            }
            formEncode(builder.build(), out);
        } catch (Exception e) {
            handleException(response, e, applicationProperties.getBaseUrl(), true);
        }
    }

    private URI callbackToUri(String callbackParameter) throws IOException, OAuthProblemException {
        if (callbackParameter.equals("oob")) {
            return null;
        }
        URI callback;
        try {
            callback = new URI(callbackParameter);
        } catch (URISyntaxException e) {
            log.error("Unable to parse callback URI '%s'", callbackParameter);
            OAuthProblemException problem = new OAuthProblemException(PARAMETER_REJECTED);
            problem.setParameter(OAUTH_PARAMETERS_REJECTED, OAUTH_CALLBACK);
            problem.setParameter(OAUTH_PROBLEM_ADVICE, String.format(INVALID_CALLBACK_ADVICE, callbackParameter));
            throw problem;
        }
        if (!ServiceProviderToken.isValidCallback(callback)) {
            log.error("Invalid callback URI '%s'", callbackParameter);
            OAuthProblemException problem = new OAuthProblemException(PARAMETER_REJECTED);
            problem.setParameter(OAUTH_PARAMETERS_REJECTED, OAUTH_CALLBACK);
            problem.setParameter(OAUTH_PROBLEM_ADVICE, String.format(INVALID_CALLBACK_ADVICE, callbackParameter));
            throw problem;
        }
        return callback;
    }
}
