package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.atlassian.oauth.serviceprovider.internal.util.UserAgentUtil;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.AuthenticationListener;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.auth.OAuthRequestVerifier;
import com.atlassian.sal.api.auth.OAuthRequestVerifierFactory;
import net.oauth.OAuthMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

import static com.atlassian.oauth.util.RequestAnnotations.markAsOAuthRequest;
import static com.google.common.base.Preconditions.checkNotNull;

public class OAuthFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(OAuthFilter.class);

    private final Authenticator authenticator;
    private final AuthenticationListener authenticationListener;
    private final AuthenticationController authenticationController;

    private final ApplicationProperties applicationProperties;
    private final OAuthRequestVerifierFactory verifierFactory;

    public OAuthFilter(Authenticator authenticator,
                       AuthenticationListener authenticationListener,
                       AuthenticationController authenticationController,
                       ApplicationProperties applicationProperties,
                       OAuthRequestVerifierFactory verifierFactory) {
        this.authenticator = checkNotNull(authenticator, "authenticator");
        this.authenticationListener = checkNotNull(authenticationListener, "authenticationListener");
        this.authenticationController = checkNotNull(authenticationController, "authenticationController");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.verifierFactory = checkNotNull(verifierFactory, "oAuthRequestVerifierFactory");
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        String userAgency = request.getHeader(UserAgentUtil.HEADER_USER_AGENT);

        if (UserAgentUtil.isOsxFinder(userAgency) || UserAgentUtil.isMicrosoftMiniRedirector(userAgency)) {
            chain.doFilter(request, res);
        } else {
            final HttpServletResponse response = new OAuthWWWAuthenticateAddingResponse((HttpServletResponse) res, applicationProperties);

            OAuthRequestVerifier verifier = verifierFactory.getInstance(req);
            boolean verifierStatus = verifier.isVerified();

            if (!mayProceed(request, response, verifier)) {
                LOG.debug("OAuth blocked the request [{}]", request.getRequestURL());
                return;
            }
            try {
                chain.doFilter(request, response);
            } finally {
                if (verifierStatus) {
                    verifier.setVerified(true);
                } else {
                    verifier.clear();
                }

                // Kill off session if this is an OAuth attempt.
                if (OAuthRequestUtils.isOAuthAccessAttempt(request) && request.getSession(false) != null) {
                    request.getSession().invalidate();
                    LOG.debug("OAuth invalidated the session for an OAuth request [{}]", request.getRequestURL());
                }
            }
        }

    }

    private boolean mayProceed(HttpServletRequest request, HttpServletResponse response, OAuthRequestVerifier verifier) {
        // is it a protected resource? if not, we don't care
        if (!authenticationController.shouldAttemptAuthentication(request)) {
            authenticationListener.authenticationNotAttempted(request, response);
            return true;
        }

        // if it's not an OAuth request, we allow the filter chain to continue being processed,
        // but we want to add the WWW-Authenticate header
        if (!OAuthRequestUtils.isOAuthAccessAttempt(request)) {
            authenticationListener.authenticationNotAttempted(request, response);
            return true;
        }

        final Authenticator.Result result = authenticator.authenticate(request, response);
        if (result.getStatus() == Authenticator.Result.Status.FAILED) {
            authenticationListener.authenticationFailure(result, request, response);
            OAuthProblemUtils.logOAuthRequest(request, "OAuth authentication FAILED.", LOG);
            return false;
        }

        if (result.getStatus() == Authenticator.Result.Status.ERROR) {
            authenticationListener.authenticationError(result, request, response);
            OAuthProblemUtils.logOAuthRequest(request, "OAuth authentication ERRORED.", LOG);
            return false;
        }

        // either 3LO or 2LO means the authentication is successful.
        authenticationListener.authenticationSuccess(result, request, response);
        // mark it as OAuth request if we proceed with this.
        markAsOAuthRequest(request);
        OAuthProblemUtils.logOAuthRequest(request, "OAuth authentication successful. Request marked as OAuth.", LOG);
        // set thread-local to confirm that request is verified
        verifier.setVerified(true);
        OAuthProblemUtils.logOAuthRequest(request, "OAuth authentication successful. Thread marked as Verified.", LOG);

        return true;
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }

    /**
     * Wraps a HttpServletResponse and listens for the status to be set to a "401 Not authorized" or a 401 error to
     * be sent so that it can add the WWW-Authenticate headers for OAuth.
     */
    private static final class OAuthWWWAuthenticateAddingResponse extends HttpServletResponseWrapper {
        private final ApplicationProperties applicationProperties;

        public OAuthWWWAuthenticateAddingResponse(HttpServletResponse response, ApplicationProperties applicationProperties) {
            super(response);
            this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        }

        @Override
        public void sendError(int sc, String msg) throws IOException {
            if (sc == SC_UNAUTHORIZED) {
                addOAuthAuthenticateHeader();
            }
            super.sendError(sc, msg);
        }

        @Override
        public void sendError(int sc) throws IOException {
            if (sc == SC_UNAUTHORIZED) {
                addOAuthAuthenticateHeader();
            }
            super.sendError(sc);
        }

        @Override
        public void setStatus(int sc, String sm) {
            if (sc == SC_UNAUTHORIZED) {
                addOAuthAuthenticateHeader();
            }
            super.setStatus(sc, sm);
        }

        @Override
        public void setStatus(int sc) {
            if (sc == SC_UNAUTHORIZED) {
                addOAuthAuthenticateHeader();
            }
            super.setStatus(sc);
        }

        private void addOAuthAuthenticateHeader() {
            try {
                OAuthMessage message = new OAuthMessage(null, null, null);
                super.addHeader("WWW-Authenticate", message.getAuthorizationHeader(applicationProperties.getBaseUrl()));
            } catch (IOException e) {
                // ignore, this will never happen
                throw new RuntimeException("Somehow the OAuth.net library threw an IOException, even though it's not doing any IO operations", e);
            }
        }
    }
}
