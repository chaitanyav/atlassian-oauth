package com.atlassian.oauth.serviceprovider.internal.servlet.user;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.apache.commons.lang.StringUtils.isEmpty;

public class IconUriServlet extends HttpServlet {
    private final RequestFactory<Request<?, ?>> requestFactory;

    public IconUriServlet(RequestFactory<Request<?, ?>> requestFactory) {
        this.requestFactory = checkNotNull(requestFactory, "requestFactory");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String consumerUri = req.getParameter("consumerUri");

        if (isEmpty(consumerUri)) {
            resp.sendError(SC_BAD_REQUEST);
        } else {
            IconUriResponseHandler responseHandler = new IconUriResponseHandler(resp);
            Request<?, ?> request = requestFactory.createRequest(Request.MethodType.GET, consumerUri);
            request.setHeader("Accept", "text/html");
            try {
                request.execute(responseHandler);
            } catch (ResponseException e) {
                resp.sendError(SC_INTERNAL_SERVER_ERROR, e.getMessage());
            }
        }
    }

    private static class IconUriResponseHandler implements ResponseHandler {
        private final HttpServletResponse resp;

        public IconUriResponseHandler(HttpServletResponse resp) {
            this.resp = resp;
        }

        @Override
        public void handle(Response response) throws ResponseException {
            if (response.getStatusCode() != SC_OK) {
                throw new ResponseException("Server responded with an error");
            }

            try {
                IOUtils.copy(response.getResponseBodyAsStream(), resp.getOutputStream());
                resp.setStatus(response.getStatusCode());
                resp.setContentType("text/html");
                IOUtils.closeQuietly(resp.getOutputStream());
            } catch (IOException e) {
                throw new ResponseException(e);
            }
        }
    }
}
