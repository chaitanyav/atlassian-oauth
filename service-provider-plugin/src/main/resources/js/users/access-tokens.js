var AJS = AJS || {};

AJS.$(function() {
    var revokeConfirmation = AJS.$("#revoke-confirmation-text").val(),
        revokeErrorMessage = AJS.$("#revoke-error-message").val(),

        revokeToken = function(token) {
            var consumerName = AJS.$("#token-" + token + "-name").text();
            if (!confirm(AJS.format(revokeConfirmation, consumerName))) {
                return;
            }
            AJS.$.ajax({
                type: "POST",
                data: { token: token },
                success: function() {
                    removeTokenItemFromList(token);
                },
                error: function() {
                    alert(revokeErrorMessage);
                }
            });
        },

        removeTokenItemFromList = function(token) {
            var tokenItem = AJS.$(".revoke[href$=" + token + "]").parents(".token-item");

            var nextDivider = tokenItem.next(".divider");
            var prevDivider = tokenItem.prev(".divider");
            if (nextDivider.length) {
                nextDivider.remove();
            } else if (prevDivider.length) {
                prevDivider.remove();
            }

            tokenItem.remove();
        },

        getConsumerUri = function(iconDiv) {
            var consumerUri = AJS.$(iconDiv).closest(".token-item").find("a.consumerUri");

            if (consumerUri && consumerUri.length) {
                return consumerUri.attr("href");
            }
            return null;
        },

        loadIcon = function(iconDiv, consumerUri) {
            var faviconUri = consumerUri + "/favicon.ico",
                faviconImg = AJS.$('<img class="remote-icon" alt="">');

            faviconImg.load(function() {
                iconDiv.removeClass("default-icon").empty();
                AJS.$(this).appendTo(iconDiv);
            });
            faviconImg.error(function() {
                findAndLoadLinkedIcon(iconDiv, consumerUri);
            });

            faviconImg.attr("src", faviconUri);
        },

        findAndLoadLinkedIcon = function(iconDiv, consumerUri) {
            AJS.$.ajax({
                type: "GET",
                url: "icon-uri?consumerUri=" + consumerUri,
                success: function(data) {
                    var iconUri = parseIconLinkUriFromHtml(data);
                    if (iconUri) {
                        iconDiv.removeClass("default-icon").empty();
                        AJS.$('<img class="remote-icon" alt="">')
                            .attr("src", makeAbsoluteUri(consumerUri, iconUri))
                            .appendTo(iconDiv);
                    }
                }
            });
        },

        parseIconLinkUriFromHtml = function(html) {
            var tagRegex = /<\s*link\s*[^>]*>/gi,
                relRegex = /\s*rel\s*=\s*"([^"]*)"/i,
                hrefRegex = /\s*href\s*=\s*"([^"]*)"/i,
                tagMatch;
            while (tagMatch = tagRegex.exec(html)) {
                var tag = tagMatch[0].replace("'", "\""),
                    relMatch = relRegex.exec(tag),
                    linkRel = relMatch && relMatch[1] && relMatch[1].toLowerCase();
                if (linkRel == 'icon' || linkRel == 'shortcut icon') {
                    var hrefMatch = hrefRegex.exec(tag);
                    if (hrefMatch) {
                        return hrefMatch[1];
                    }
                }
            }
            return null;
        },

        isAbsolute = function(uri) {
            return uri && (uri.toLowerCase().indexOf("http://") === 0 || uri.toLowerCase().indexOf("https://") === 0);
        },

        makeAbsoluteUri = function(baseUrl, relativeUri) {
            if (isAbsolute(relativeUri) || !isAbsolute(baseUrl)) {
                return relativeUri;
            }
            if (relativeUri.indexOf("/") === 0) {
                return baseUrl.substring(0, baseUrl.indexOf("/", "https://".length)) + relativeUri;
            } else {
                return baseUrl + "/" + relativeUri;
            }
        };

    AJS.$(".revoke").each(function() {
        AJS.$(this).click(function(event) {
            revokeToken(this.href.substring(this.href.lastIndexOf("#") + 1));
            event.preventDefault();
        });
    });

    AJS.$(".app-icon").each(function() {
        var iconDiv = AJS.$(this),
            consumerUri = getConsumerUri(iconDiv);

        if (consumerUri) {
            loadIcon(iconDiv, consumerUri);
        }
    });
});
