package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static net.oauth.OAuth.OAUTH_NONCE;
import static net.oauth.OAuth.OAUTH_SIGNATURE;
import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.OAUTH_TIMESTAMP;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OAuthRequestUtilsTest {
    private static final Map<String, String[]> OAUTH_PARAMS = ImmutableMap.<String, String[]>builder()
            .put(OAUTH_CONSUMER_KEY, new String[]{"consumer-key"})
            .put(OAUTH_TOKEN, new String[]{"1234"})
            .put(OAUTH_SIGNATURE_METHOD, new String[]{"RSA-SHA1"})
            .put(OAUTH_SIGNATURE, new String[]{"signature"})
            .put(OAUTH_TIMESTAMP, new String[]{"1111111111"})
            .put(OAUTH_NONCE, new String[]{"1"})
            .build();

    @Mock
    HttpServletRequest request;

    @Test
    public void verifyThatRequestWithNonEmptyOAuthTokenIsNot2LO() throws Exception {
        setupRequest(OAUTH_PARAMS);
        assertThat(OAuthRequestUtils.is2LOAuthAccessAttempt(request), is(equalTo(false)));
    }

    @Test
    public void verifyThatRequestWithEmptyOAuthTokenIs2LO() throws Exception {
        Map<String, String[]> params = Maps.newHashMap(OAUTH_PARAMS);
        params.put(OAUTH_TOKEN, new String[]{""});
        setupRequest(params);
        assertThat(OAuthRequestUtils.is2LOAuthAccessAttempt(request), is(equalTo(true)));
    }

    @Test
    public void verifyThatRequestWithNullOAuthTokenIsNot2LO() throws Exception {
        Map<String, String[]> params = Maps.newHashMap(OAUTH_PARAMS);
        params.remove(OAUTH_TOKEN);
        setupRequest(params);
        assertThat(OAuthRequestUtils.is2LOAuthAccessAttempt(request), is(equalTo(false)));
    }

    @Test
    public void verifyThatRequestWithNonEmptyOAuthTokenIsNot3LO() throws Exception {
        setupRequest(OAUTH_PARAMS);
        assertThat(OAuthRequestUtils.is3LOAuthAccessAttempt(request), is(equalTo(true)));
    }

    @Test
    public void verifyThatRequestWithEmptyOAuthTokenIsNot3LO() throws Exception {
        Map<String, String[]> params = Maps.newHashMap(OAUTH_PARAMS);
        params.put(OAUTH_TOKEN, new String[]{""});
        setupRequest(params);
        assertThat(OAuthRequestUtils.is3LOAuthAccessAttempt(request), is(equalTo(false)));
    }

    @Test
    public void verifyThatRequestWithNullOAuthTokenIsNot3LO() throws Exception {
        Map<String, String[]> params = Maps.newHashMap(OAUTH_PARAMS);
        params.remove(OAUTH_TOKEN);
        setupRequest(params);
        assertThat(OAuthRequestUtils.is3LOAuthAccessAttempt(request), is(equalTo(false)));
    }

    @Test
    public void verifyThatRequestWithNoOAuthTokenIsNeither2LOor3LO() throws Exception {
        Map<String, String[]> params = Maps.newHashMap(OAUTH_PARAMS);
        params.remove(OAUTH_TOKEN);
        setupRequest(params);
        assertThat(OAuthRequestUtils.is2LOAuthAccessAttempt(request), is(equalTo(false)));
        assertThat(OAuthRequestUtils.is3LOAuthAccessAttempt(request), is(equalTo(false)));
        assertThat(OAuthRequestUtils.isOAuthAccessAttempt(request), is(equalTo(false)));
    }

    private void setupRequest(Map<String, String[]> params) {
        when(request.getRequestURI()).thenReturn("http://service/resource");
        when(request.getContextPath()).thenReturn("http://service");
        when(request.getParameterMap()).thenReturn(params);
        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            when(request.getParameter(entry.getKey())).thenReturn(entry.getValue()[0]);
        }
    }
}
