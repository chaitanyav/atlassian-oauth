package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version;
import com.atlassian.oauth.serviceprovider.internal.servlet.authorize.AuthorizationRendererImpl.Renderable;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;

import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationRendererImplTest {
    static final ServiceProviderToken TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(Version.V_1_0_A).build();

    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    TemplateRenderer templateRenderer;
    @Mock
    ConsumerInformationRenderer firstConsumerInfoRenderer;
    @Mock
    ConsumerInformationRenderer secondConsumerInfoRenderer;
    @Mock
    ConsumerInformationRenderer thirdConsumerInfoRenderer;
    @Mock
    ConsumerInformationRenderer basicConsumerInformationRenderer;

    AuthorizationRenderer renderer;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @Before
    public void setUp() throws Exception {
        when(response.getWriter()).thenReturn(new PrintWriter(new ByteArrayOutputStream()));
        when(applicationProperties.getBaseUrl()).thenReturn("http://base");

        doAnswer(invokeConsumerRenderer())
                .when(templateRenderer).render(anyString(), anyMap(String.class, Object.class), isA(Writer.class));

        Iterable<ConsumerInformationRenderer> consumerInfoRenderers = ImmutableList.of(
                firstConsumerInfoRenderer, secondConsumerInfoRenderer, thirdConsumerInfoRenderer);
        renderer = new AuthorizationRendererImpl(applicationProperties,
                templateRenderer, consumerInfoRenderers, basicConsumerInformationRenderer);
    }

    @Test
    public void verifyThatBasicConsumerInformationRendererIsUsedWhenNoneOthersCanRenderConsumerInformation() throws Exception {
        renderer.render(request, response, TOKEN);

        verify(basicConsumerInformationRenderer).render(eq(TOKEN), eq(request), isA(Writer.class));
    }

    @Test
    public void verifyThatSelectedConsumerInformationRendererIsUsedToRenderConsumerInforamtion() throws Exception {
        when(secondConsumerInfoRenderer.canRender(TOKEN, request)).thenReturn(true);

        renderer.render(request, response, TOKEN);

        verify(secondConsumerInfoRenderer).render(eq(TOKEN), eq(request), isA(Writer.class));
    }

    @Test
    public void verifyThatSelectedConsumerInformationRendererThrowingServiceUnavailableExceptionCausesFallbackToBasicRenderer() throws Exception {
        when(secondConsumerInfoRenderer.canRender(TOKEN, request)).thenReturn(true);
        doThrow(new ServiceUnavailableException())
                .when(secondConsumerInfoRenderer).render(eq(TOKEN), eq(request), isA(Writer.class));

        renderer.render(request, response, TOKEN);

        verify(basicConsumerInformationRenderer).render(eq(TOKEN), eq(request), isA(Writer.class));
    }

    @SuppressWarnings("unchecked")
    private static <K, V> Map<K, V> anyMap(Class<K> k, Class<V> v) {
        return Mockito.anyMap();
    }

    private static Answer<Void> invokeConsumerRenderer() {
        return ConsumerRendererInvoker.INSTANCE;
    }

    private static enum ConsumerRendererInvoker implements Answer<Void> {
        INSTANCE;

        public Void answer(InvocationOnMock invocation) throws Throwable {
            @SuppressWarnings("unchecked")
            Map<String, Object> context = (Map<String, Object>) invocation.getArguments()[1];

            ((Renderable) context.get("consumerRenderer")).render();
            return null;
        }
    }

    private static class ServiceUnavailableException extends RuntimeException {
    }
}
