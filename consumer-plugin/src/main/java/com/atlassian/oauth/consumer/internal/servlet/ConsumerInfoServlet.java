package com.atlassian.oauth.consumer.internal.servlet;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerCreationException;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConsumerInfoServlet extends HttpServlet {
    private final ConsumerService store;
    private final TemplateRenderer renderer;

    public ConsumerInfoServlet(ConsumerService store, TemplateRenderer renderer) {
        this.store = checkNotNull(store, "store");
        this.renderer = checkNotNull(renderer, "renderer");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/xml;charset=UTF-8");
        Consumer consumer = store.getConsumer();
        Map<String, Object> context = ImmutableMap.of(
                "consumer", consumer,
                "encodedPublicKey", RSAKeys.toPemEncoding(consumer.getPublicKey())
        );
        try {
            renderer.render("view.xml.vm", context, response.getWriter());
        } catch (RenderingException e) {
            throw new ServletException(e);
        } catch (ConsumerCreationException e) {
            throw new ServletException(e);
        }
    }
}
